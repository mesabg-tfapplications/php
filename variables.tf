variable "environment" {
  type        = string
  description = "Environment name"
}

variable "application_name" {
  type        = string
  description = "General project name"
}

variable "project_name" {
  type        = string
  description = "General project name"
}

variable "cluster_name" {
  type        = string
  description = "Cluster name"
}

variable "s3_environment_file" {
  type        = string
  description = "S3 ARN for environment variables"
}

variable "phpfpm_image" {
  type        = string
  description = "Default image to use for PHPFPM"
  default     = "bitnami/php-fpm:7.4"
}

variable "nginx_image" {
  type        = string
  description = "Default image to use for NGINX"
  default     = "nginx:1.19.4"
}

#variable "dns_resolver" {
#  type        = string
#  description = "DNS resolver for nginx service locator"
#  default     = "10.0.0.2"
#}

variable "domain" {
  type        = string
  description = "Domain to deploy"
}

variable "zone_id" {
  type        = string
  description = "Route53 zone to register the domain"
}

variable "vpc_id" {
  type        = string
  description = "VPC identifier"
}

variable "subnet_ids" {
  type        = list(string)
  description = "Subnet identifiers"
}

variable "security_group" {
  type        = string
  description = "Security Group to assign"
}

variable "certificate_arn" {
  type        = string
  description = "Certificate ARN for HTTPS usage"
}

variable "auto_scaling_role_arn" {
  type        = string
  description = "AutoScaling role for application"
}

variable "task_execution_role_arn" {
  type        = string
  description = "TaskExecution role for application"
}

variable "log_group" {
  type        = string
  description = "Log Group Name"
}

variable "namespace_id" {
  type        = string
  description = "CloudMap namespace identifier"
}

variable "namespace_name" {
  type        = string
  description = "CloudMap namespace name"
}

variable "max_capacity" {
  type        = number
  description = "max_capacity"
  default     = 4
}

variable "cpu" {
  type        = number
  description = "Number of CPU units to assign to the proccess"
  default     = 256
}

variable "memory" {
  type        = number
  description = "Amount of memory to assign to the proccess"
  default     = 512
}
