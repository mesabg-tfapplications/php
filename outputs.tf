output "dns_name" {
  value       = "${var.application_name}.${var.namespace_name}"
  description = "Resource DNS name"
}
