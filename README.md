# PHP

This module is capable to deploy any PHP project, with the usage of 2 containers, nginx and php-fpm

Module Input Variables
----------------------

- `environment` - environment name
- `application_name` - application_name
- `project_name` - general project name
- `cluster_name` - cluster name
- `s3_environment_file` - s3 arn for environment variables loading
- `dns_resolver` - dns resolver for nginx service locator (10.0.0.2)
- `vpc_id` - vpc identifier
- `domain` - application domain to be registered
- `zone_id` - Route53 regitrar
- `subnet_ids` - subnets identifiers
- `security_group` - security group to be applied on the application
- `certificate_arn` - to allow https ussage on alb
- `autoscaling_role_arn` - autoscaling role
- `task_execution_role_arn` - task execution role
- `log_group` - to store the logs
- `namespace_id` - cloud map namespace identifier
- `namespace_name` - cloud map namespace domain name
- `max_capacity` - cluster replication max capacity (default 4)
- `cpu` - cpu units to allocate (default 256)
- `memory` - memory to allocate (default 512)

Usage
-----

```hcl
module "php" {
  source                  = "git::https://gitlab.com/mesabg-tfapplications/php.git"

  environment             = "environment"
  application_name        = "someapp"
  project_name            = "name"
  cluster_name            = "cluster"
  s3_environment_file     = "arn:some/file.env"

  dns_resolver            = "10.0.0.2"
  vpc_id                  = "vpc-xxx"
  domain                  = "something.com"
  zone_id                 = "domain-xxxx"
  subnet_ids              = ["subnet-xxxx"]
  security_group          = "sg-xxxx"
  certificate_arn         = "cert:arn"
  
  auto_scaling_role_arn   = "iam::role"
  task_execution_role_arn = "iam::role"
  log_group               = "/some/group"

  namespace_id            = "ns-xxxx"
  namespace_name          = "default.domain"

  max_capacity            = 10
  cpu                     = 1024
  memory                  = 2048
}
```

Outputs
=======

 - `dns_name` - Domain where the application is hosted


Authors
=======
##### Moisés Berenguer <moises.berenguer@gmail.com>
