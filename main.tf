terraform {
  required_version = ">= 0.13.2"
}

/*
module "volume" {
  source            = "git::https://gitlab.com/mesabg-tfmodules/file_system.git?ref=v1.0.0"

  name              = "${var.project_name}_${var.application_name}_php_${var.environment}"
  environment       = var.environment
  persistent        = true
  path              = "/"

  subnet_ids        = var.subnet_ids
  security_groups   = [var.security_group]
}
*/

data "aws_ecs_cluster" "ecs_cluster" {
  cluster_name = var.cluster_name
}

resource "aws_lb" "lb" {
  name                       = "${var.project_name}-${var.application_name}-${var.environment}"
  internal                   = false
  load_balancer_type         = "application"
  security_groups            = [var.security_group]
  subnets                    = var.subnet_ids

  tags = {
    Name                     = "${var.project_name}-${var.application_name}-${var.environment}"
    Environment              = var.environment
  }
}

resource "aws_route53_record" "route53_record" {
  depends_on                = [aws_lb.lb]
  zone_id                   = var.zone_id
  name                      = var.domain
  type                      = "A"

  alias {
    name                    = aws_lb.lb.dns_name
    zone_id                 = aws_lb.lb.zone_id
    evaluate_target_health  = true
  }
}

resource "aws_cloudformation_stack" "php" {
  depends_on                      = [aws_lb.lb]
  name                            = "${var.project_name}-${var.application_name}-${var.environment}"

  parameters    = {
    ProjectName                   = "${var.project_name}-${var.environment}"
    ApplicationName               = var.application_name
    EnvironmentName               = var.environment
    ClusterArn                    = data.aws_ecs_cluster.ecs_cluster.arn
    LogGroupName                  = var.log_group
    EnvironmentArn                = var.s3_environment_file
    PhpFpmImage                   = var.phpfpm_image
    NginxImage                    = var.nginx_image
    #DnsResolver                   = var.dns_resolver
    #DomainName                    = var.domain
    VpcId                         = var.vpc_id
    Subnets                       = join(",", var.subnet_ids)
    LoadBalancerArn               = aws_lb.lb.arn
    CertificateArn                = var.certificate_arn
    CloudMapNamespaceId           = var.namespace_id
    CloudMapNamespaceName         = var.namespace_name
    SecurityGroup                 = var.security_group
    AutoScalingRoleARN            = var.auto_scaling_role_arn
    TaskExecutionRoleARN          = var.task_execution_role_arn
    #VolumeFileSystemId            = module.volume.file_system_id
    #VolumeFileSystemAccessPointId = module.volume.access_point_id
    MaxCapacity                   = tostring(var.max_capacity)
    Cpu                           = tostring(var.cpu)
    Memory                        = tostring(var.memory)
  }

  capabilities  = ["CAPABILITY_IAM", "CAPABILITY_NAMED_IAM", "CAPABILITY_AUTO_EXPAND"]
  on_failure    = "DELETE"

  template_body = file("${path.module}/stack.json")
}
